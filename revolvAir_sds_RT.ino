// G. Simard & Mateo - 2018
//
#include <Arduino_FreeRTOS.h>

#include "hackair.h"
hackAIR sensor(SENSOR_SDS011);
struct hackAirData data;

// Pour LCD 16x2 i2c
#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C  lcd(0x27, 2, 1, 0, 4, 5, 6, 7); // 0x27 is the I2C bus address for an unmodified module


// Tâches
void TaskBlink( void *pvParameters );
void TaskAnalogRead( void *pvParameters );
void TaskDisplayLCD(void *pvParameters);


void setup() {
  
  Serial.begin(9600);
  
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB, on LEONARDO, MICRO, YUN, and other 32u4 based boards.
  }

  setupLCD();
  Serial.println("Setup LCD Done");

  setupSDS();
  Serial.println("Setup SDS DONE");


  // Now set up two tasks to run independently.
  xTaskCreate(
    TaskBlink
    ,  (const portCHAR *)"Blink"   // A name just for humans
    ,  128  // This stack size can be checked & adjusted by reading the Stack Highwater
    ,  NULL
    ,  2  // Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
    ,  NULL );

  xTaskCreate(TaskAnalogRead,  (const portCHAR *) "AnalogRead",  128, NULL, 1, NULL );

  //xTaskCreate(TaskDisplayLCD,  (const portCHAR *)"DisplayLCD", 128, NULL, 0, NULL);

  Serial.println("Tasks Created");

  // Now the task scheduler, which takes over control of scheduling individual tasks, is automatically started.
}

void loop()
{
  // Empty. Things are done in Tasks.
}


void TaskDisplayLCD(void *pvParameters)
{
  Serial.println("Task LCD");
  (void) pvParameters;
  
  for (;;)
  {
    afficheLCD();
    Serial.println("Task LCD ... printing ....");
    vTaskDelay( 2000 / portTICK_PERIOD_MS );
  }
}


void TaskBlink(void *pvParameters)
{
  Serial.println("Task Blink");
  (void) pvParameters;

  pinMode(LED_BUILTIN, OUTPUT);

  for (;;) // A Task shall never return or exit.
  {
    digitalWrite(LED_BUILTIN, HIGH);
    vTaskDelay( 1000 / portTICK_PERIOD_MS ); // wait for one second
    digitalWrite(LED_BUILTIN, LOW);
    vTaskDelay( 1000 / portTICK_PERIOD_MS );
  }
}

void TaskAnalogRead(void *pvParameters)
{
  Serial.println("Task Read");
  (void) pvParameters;

  for (;;)
  {
    afficheLCD();
     Serial.println("sensor ON");
     sensor.turnOn();
     vTaskDelay(10000 / portTICK_PERIOD_MS ); // 10 sec.
    
     Serial.println("sensor Refresh");
     sensor.clearData(data);
     
     sensor.refresh(data);
     while(data.error != 0)
     {
       sensor.refresh(data);
       delay(100);
     }

     afficheLCD();
     delay(100);
     
     afficheSerial();
     
     Serial.println("sensor OFF");
     sensor.turnOff(); 
     vTaskDelay(40000 / portTICK_PERIOD_MS ); // 40 sec.
    
     vTaskDelay(1);  // one tick delay (15ms) in between reads for stability
  } 
}


void setupSDS() 
{
  sensor.begin();
  sensor.enablePowerControl();
  sensor.turnOn();
  sensor.clearData(data);
}


void afficheSerial()
{
  if (data.error != 0) {
    Serial.println("sds011 Data Error = 0!");
    Serial.println(data.error);
  } 
  else 
  {
    Serial.print("PM2.5: ");
    Serial.println(data.pm25);
    Serial.print("PM10: ");
    Serial.println(data.pm10);
  }
}

void setupLCD() 
{
  lcd.begin (16, 2);
  lcd.setBacklightPin (3, POSITIVE);
  lcd.setBacklight(HIGH);
  lcd.setCursor (0, 1);  // go to start of 2nd line

  lcd.print("Mateo GuS");
  delay(1000);
  lcd.clear();
}

void afficheLCD()
{
  Serial.println("afficheLCD()");

  if (data.error != 0) {
    lcd.clear();
    lcd.setCursor(10, 0);
    lcd.print("Err");
    lcd.setCursor(10, 1);
    lcd.print("Err");
  } else {
    lcd.clear();
    lcd.setCursor(10, 0);
    lcd.print(data.pm10);
    lcd.setCursor(10, 1);
    lcd.print(data.pm25);
  }
}


